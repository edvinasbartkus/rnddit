import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import If from '../../utils/If';

export default class PostView extends Component {
  render() {
    const data = this.props.post.item.data;
    return (
      <TouchableOpacity onPress={() => this.props.onOpen()}>
        <View style={styles.post}>
          <If condition={data.thumbnail}>
            <Image
              source={{uri: data.thumbnail}}
              style={{width: data.thumbnail_width, height: data.thumbnail_height}}
            />
          </If>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{data.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  post: {
    flexDirection: 'row',
    padding: 15
  },

  titleContainer: {
    flex: 1,
    paddingLeft: 10
  },

  title: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 22
  }
})
