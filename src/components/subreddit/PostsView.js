import React, { Component } from 'react';
import PostView from './PostView';
import {
  View,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  Text
} from 'react-native';
import { SearchBar } from 'react-native-elements'
import { searchText } from '../../utils/search';

export default class PostsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: null
    }
  }

  onOpen(post) {
    this.props.navigator.push({
      screen: 'rnddit.PostScreen',
      passProps: { post }
    });
  }

  getPosts() {
    if (this.state.search && this.state.search.length > 3) {
      return searchText(this.props.posts, this.state.search);
    } else {
      return this.props.posts;
    }
  }

  renderItem(post) {
    return (
      <PostView
        post={post}
        key={post.item.data.created}
        onOpen={() => this.onOpen(post)}
      />
    );
  }

  renderSeparator() {
    return (
      <View style={styles.separator} />
    );
  }

  renderHeader() {
    return (
      <SearchBar
        round
        lightTheme
        value={this.state.search}
        onChangeText={(text) => this.setState({ search: text })}
        placeholder='Search Here...'
      />
    );
  }

  renderFooter() {
    if (this.props.isFetching) {
      return (
        <ActivityIndicator size={'large'} style={styles.indicator} />
      );
    } else if (this.props.posts.length == 0) {
      return (
        <View style={styles.footer}>
          <Text>No posts here!</Text>
        </View>
      );
    } else {
      return (
        <View />
      );
    }
  }

  render() {
    return (
      <View>
        {this.renderHeader()}
        <FlatList
          data={this.getPosts()}
          renderItem={(item) => this.renderItem(item)}
          keyExtractor={(item, index) => index}
          ItemSeparatorComponent={() => this.renderSeparator()}
          ListFooterComponent={() => this.renderFooter()}
          {...this.props}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    borderTopWidth: 1,
    borderTopColor: '#F6F6F6'
  },
  indicator: {
    padding: 20
  },
  footer: {
    padding: 20,
    alignItems: 'center'
  }
});
