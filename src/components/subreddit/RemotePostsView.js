import React, { Component } from 'react';
import { View } from 'react-native';
import PostsView from './PostsView';
import { connect } from 'react-redux';
import * as postsActions from '../../redux/posts/actions';

class RemotePostsView extends Component {
  componentWillMount() {
    this.props.dispatch(postsActions.load(this.props.subreddit));
  }

  onEndReached() {
    if (!this.props.isFetching) {
      const length = this.props.posts.length;
      const lastPost = this.props.posts[length-1];
      this.props.dispatch(postsActions.load(this.props.subreddit, lastPost.data.name));
    }
  }

  render() {
    const { props } = this;
    return (
      <PostsView
        posts={this.props.posts}
        onEndReachedTreshold={0.5}
        onEndReached={() => this.onEndReached()}
        {...props}
      />
    )
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.posts.isFetching,
    posts: state.posts.posts
  }
}

export default connect(mapStateToProps)(RemotePostsView);
