import PostsView from './PostsView';
import RemotePostsView from './RemotePostsView';

export {
  PostsView,
  RemotePostsView
}
