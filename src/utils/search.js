import _ from 'underscore';

export function searchText(posts, query) {
  const q = query.toLowerCase();
  return _.filter(posts, (item) => {
    return item.data.title.toLowerCase().search(q) >= 0;
  });
}
