const WHITE = '#FFFFFF';
const BLUE = '#1081E0';
export const NAVIGATOR_STYLE = {
  navBarButtonColor: WHITE,
  navBarTextColor: WHITE,
  statusBarTextColorScheme: 'light',
  statusBarTextColorSchemeSingleScreen: 'light',
  navBarBackgroundColor: BLUE
}

export const REDDIT_DOMAIN = 'https://www.reddit.com';
export const DEFAULT_SUBREDDIT = 'aww';
export const redditUrl = (subreddit, last) => {
  let url = `${REDDIT_DOMAIN}/r/${subreddit}.json`;
  if (last) {
    url = `${url}?after=${last}`
  }

  return url;
}
