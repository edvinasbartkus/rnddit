import React, { Component } from 'react';
import { View } from 'react-native';

export default class If extends Component {
  render() {
    return (
      <View>{this.props.condition ? this.props.children : null}</View>
    );
  }
}
