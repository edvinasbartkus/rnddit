import { createStore, applyMiddleware } from 'redux';
import rootReducer from './rootReducer';
import thunk from 'redux-thunk';

let middleware = [thunk];

export default function configureStore(initialStore) {
  return createStore(
    rootReducer,
    initialStore,
    applyMiddleware(...middleware)
  );
}
