import { combineReducers } from 'redux';
import favorites from './favorites/reducer';
import posts from './posts/reducer';

const rootReducer = combineReducers({
  favorites,
  posts
});

export default rootReducer;
