import * as types from './types';
import { redditUrl } from '../../utils/constants';

export function load(subreddit, last = undefined) {
  return dispatch => {
    dispatch(request());

    return fetch(redditUrl(subreddit, last))
      .then(response => response.json())
      .then(json => dispatch(success(json)))
      .catch(error => dispatch(failure(error)))
  }
}

function request() {
  return { type: types.REQUEST };
}

function success(data) {
  return { type: types.SUCCESS, payload: data };
}

function failure(error) {
  return { type: types.FAILURE, error };
}
