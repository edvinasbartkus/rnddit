import * as types from './types';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
  isFetching: false,
  posts: [],
  error: undefined
});

export default function(state = initialState, action) {
  switch (action.type) {
    case types.REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        error: null
      });

    case types.SUCCESS:
      const newPosts = action.payload.data.children;
      return Object.assign({}, state, {
        isFetching: false,
        posts: state.posts.concat(newPosts)
      });

    case types.FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });

    default:
      return state;
  }
}
