import * as types from './types';

export function add(post) {
  return { type: types.ADD, post: post };
}

export function remove(name) {
  return { type: types.REMOVE, name: name }
}
