import * as types from './types';
import Immutable from 'seamless-immutable';
import _ from 'underscore';

const initialState = Immutable({
  favorites: []
});

export default function(state = initialState, action) {
  switch (action.type) {
    case types.ADD:
      return Object.assign({}, state, {
        favorites: state.favorites.concat(action.post)
      });

    case types.REMOVE:
      return Object.assign({}, state, {
        favorites: _.reject(state.favorites, (item) => item.data.name == action.name)
      });

    default:
      return state;
  }
}
