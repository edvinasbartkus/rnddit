import React, { Component } from 'react';
import { View, WebView } from 'react-native';
import { REDDIT_DOMAIN } from '../../utils/constants';
import { connect } from 'react-redux';
import _ from 'underscore';
import * as favoritesActions from '../../redux/favorites/actions';
import { NAVIGATOR_STYLE } from '../../utils/constants';

const FAVORITE_BUTTON = {
  title: 'Favorite',
  id: 'favorite',
  testID: 'favorite',
}

const UNFAVORITE_BUTTON = {
  title: 'Unfavorite',
  id: 'unfavorite',
  testID: 'unfavorite',
}

export class PostScreen extends Component {
  static navigatorStyle = Object.assign({}, NAVIGATOR_STYLE, {
    tabBarHidden: true
  });

  componentDidMount() {
    this.props.navigator.setOnNavigatorEvent((event) => {
      return this.onNavigatorEvent(event)
    });

    this.renderButtons();
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'favorite') {
        this.props.dispatch(favoritesActions.add(this.props.post.item));
      } else if (event.id == 'unfavorite') {
        const { name } = this.props.post.item.data;
        this.props.dispatch(favoritesActions.remove(name));
      }
    }
  }

  renderButtons(props = this.props) {
    const button = props.isFavorited ? UNFAVORITE_BUTTON : FAVORITE_BUTTON;
    this.props.navigator.setButtons({
      rightButtons: [ button ],
      animated: true
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isFavorited != nextProps.isFavorited) {
      this.renderButtons(nextProps);
    }
  }

  render() {
    const { permalink } = this.props.post.item.data;
    return (
      <WebView
        source={{ uri: `${REDDIT_DOMAIN}${permalink}` }}
      />
    );
  }
}

function mapStateToProps(state, ownProps) {
  const { name } = ownProps.post.item.data;
  const isFavorited = _.chain(state.favorites.favorites)
                       .map((item) => item.data.name)
                       .contains(name)
                       .value();

  return {
    isFavorited: isFavorited
  }
}

export default connect(mapStateToProps)(PostScreen);
