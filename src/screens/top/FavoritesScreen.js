import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { PostsView } from '../../components/subreddit';
import { NAVIGATOR_STYLE } from '../../utils/constants';

class FavoritesScreen extends Component {
  static navigatorStyle = NAVIGATOR_STYLE;

  render() {
    return (
      <PostsView
        posts={this.props.posts}
        navigator={this.props.navigator}
      />
    )
  }
}

function mapStateToProps(state) {
  return {
    posts: state.favorites.favorites
  }
}

export default connect(mapStateToProps)(FavoritesScreen);
