import React, { Component } from 'react';
import { View } from 'react-native';
import { RemotePostsView } from '../../components/subreddit';
import { NAVIGATOR_STYLE } from '../../utils/constants';

export default class SubredditScreen extends Component {
  static navigatorStyle = NAVIGATOR_STYLE;

  render() {
    return (
      <RemotePostsView
        navigator={this.props.navigator}
        subreddit={this.props.subreddit}
      />
    );
  }
}
