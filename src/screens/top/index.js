import FavoritesScreen from './FavoritesScreen';
import SubredditScreen from './SubredditScreen';

export {
  FavoritesScreen,
  SubredditScreen
}
