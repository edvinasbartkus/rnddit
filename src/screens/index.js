import { Navigation } from 'react-native-navigation';
import * as top from './top';
import * as subreddit from './subreddit';

export function registerScreens(store, Provider) {
  // Top level screens
  Navigation.registerComponent('rnddit.FavoritesScreen', () => top.FavoritesScreen, store, Provider);
  Navigation.registerComponent('rnddit.SubredditScreen', () => top.SubredditScreen, store, Provider);

  // 1-st level screens
  Navigation.registerComponent('rnddit.PostScreen', () => subreddit.PostScreen, store, Provider);
}
