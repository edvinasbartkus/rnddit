import { Navigation } from 'react-native-navigation';
import { registerScreens } from './screens';
import { configureStore } from './redux';
import { Provider } from 'react-redux';
import { DEFAULT_SUBREDDIT } from './utils/constants';

const store = configureStore();
registerScreens(store, Provider);

Navigation.startTabBasedApp({
  tabs: [
    {
      label: DEFAULT_SUBREDDIT,
      screen: 'rnddit.SubredditScreen',
      title: DEFAULT_SUBREDDIT,
      icon: require('./assets/icons/Home.png'),
      selectedIcon: require('./assets/icons/HomeSelected.png'),
      passProps: {
        subreddit: DEFAULT_SUBREDDIT
      }
    },
    {
      label: 'Favorites',
      screen: 'rnddit.FavoritesScreen',
      title: 'Favorites',
      icon: require('./assets/icons/Favorites.png'),
      selectedIcon: require('./assets/icons/FavoritesSelected.png')
    }
  ]
});
