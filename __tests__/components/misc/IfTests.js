import React from 'react';
import { View, Text } from 'react-native';
import { shallow, configure } from 'enzyme';
import ReactSixteenAdapter from 'enzyme/build/adapters/ReactSixteenAdapter';
import If from '../../../src/utils/If';


configure({ adapter: new ReactSixteenAdapter() });

const POST = {
  item: {
    data: {
      permalink: '/r/aww'
    }
  }
};

describe('If', () => {
  it('renders children if conditions are true', () => {
    const wrapper = shallow(
      <If condition={true}>
        <Text />
      </If>
    );

    expect(wrapper.length).toEqual(1);
    expect(wrapper.name()).toEqual('View');
  });

  it('renders emptyness if conditions are false', () => {
    const wrapper = shallow(
      <If condition={false}>
        <Text />
      </If>
    );

    expect(wrapper.childAt(0).html()).toEqual(null)
  });
});
