import React from 'react';
import { Image } from 'react-native';
import { shallow, configure } from 'enzyme';
import ReactSixteenAdapter from 'enzyme/build/adapters/ReactSixteenAdapter';
import { FlatList } from 'react-native';
import PostView from '../../../src/components/subreddit/PostView';

configure({ adapter: new ReactSixteenAdapter() });
const POST = {
  item: {
    data: {
      thumbnail: 'image',
      thumbnail_width: 100,
      thumbnail_height: 100
    }
  }
}

describe('PostView component', () => {
  it('renders Post', () => {
    const wrapper = shallow(
      <PostView post={POST} />
    );

    expect(wrapper.length).toEqual(1);
    expect(wrapper.find(Image).length).toEqual(1);
  });
});
