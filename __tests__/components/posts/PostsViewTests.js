import React from 'react';
import { shallow, configure } from 'enzyme';
import ReactSixteenAdapter from 'enzyme/build/adapters/ReactSixteenAdapter';
import { FlatList } from 'react-native';
import { PostsView } from '../../../src/components/subreddit';
import { configureStore } from '../../../src/redux';

global.fetch = jest.fn(() => new Promise(resolve => resolve()));
configure({ adapter: new ReactSixteenAdapter() });

const POSTS = [];

describe('PostsView component', () => {
  it('renders FlatList', () => {
    const wrapper = shallow(
      <PostsView posts={POSTS} store={configureStore()} />
    );

    expect(wrapper.length).toEqual(1);
  });
});
