import * as types from '../../../src/redux/favorites/types';
import reducer from '../../../src/redux/favorites/reducer';

describe('favorites reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      favorites: []
    });
  });

  it('should handle ADD action', () => {
    const action = { type: types.ADD, post: { name: 1 } };
    expect(reducer(undefined, action)).toEqual({
      favorites: [
        { name: 1 }
      ]
    });
  });

  it('should handle REMOVE action', () => {
    const action = { type: types.REMOVE, name: 2 };
    const initialState = {
      favorites: [
        { data: { name: 1 } },
        { data: { name: 2 } },
        { data: { name: 3 } }
      ]
    };

    expect(reducer(initialState, action)).toEqual({
      favorites: [
        { data: { name: 1 } },
        { data: { name: 3 } }
      ]
    });
  });
});
