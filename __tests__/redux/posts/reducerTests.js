import * as types from '../../../src/redux/posts/types';
import reducer from '../../../src/redux/posts/reducer';

const SUCCESS_RESPONSE_CONTENT = {
  data: {
    children: [
      1
    ]
  }
}

describe('posts reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      isFetching: false,
      posts: [],
      error: undefined
    });
  });

  it('should handle REQEST', () => {
    expect(reducer(undefined, { type: types.REQUEST })).toEqual({
      isFetching: true,
      posts: [],
      error: null
    });
  });

  it('should handle SUCCESS', () => {
    const action = { type: types.SUCCESS, payload: SUCCESS_RESPONSE_CONTENT }
    expect(reducer(undefined, action)).toEqual({
      isFetching: false,
      posts: [ 1 ],
      error: undefined
    });
  });

  it('should handle FAILURE', () => {
    const action = { type: types.FAILURE, error: { message: 'Failure' }};
    expect(reducer(undefined, action)).toEqual({
      isFetching: false,
      posts: [],
      error: { message: 'Failure' }
    });
  });
});
