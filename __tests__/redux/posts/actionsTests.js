import * as actions from '../../../src/redux/posts/actions';
import * as types from '../../../src/redux/posts/types';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

const REDDIT_SUCCESS_RESPONSE = {

}

const middlewares = [ thunk ];
const mockStore = configureStore(middlewares);

describe('posts actions', () => {
  it('should call reddit.com and dispatch success event after successful request', () => {
    global.fetch = jest.fn().mockImplementation(() => {
      return new Promise((resolve, reject) => {
        resolve({
          json: () => JSON.stringify(REDDIT_SUCCESS_RESPONSE)
        });
      });
    });

    const store = mockStore({});
    return store.dispatch(actions.load('aww')).then(() => {
      const expectedActions = store.getActions();
      expect(expectedActions.length).toBe(2);
      expect(expectedActions).toContainEqual({ type: types.REQUEST });
      expect(expectedActions).toContainEqual({
        type: types.SUCCESS,
        payload: JSON.stringify(REDDIT_SUCCESS_RESPONSE)
      });
    });
  });

  it('should call reddit.com and dispatch failure event as request fails', () => {
    global.fetch = jest.fn().mockImplementation(() => {
      return new Promise((resolve, reject) => {
        reject({
          error: { message: 'Failure' }
        });
      });
    });

    const store = mockStore({});
    return store.dispatch(actions.load('aww')).then(() => {
      const expectedActions = store.getActions();
      expect(expectedActions.length).toBe(2);
      expect(expectedActions).toContainEqual({ type: types.REQUEST });
      expect(expectedActions).toContainEqual({
        type: types.FAILURE,
        error: {
          error: { message: 'Failure' }
        }
      });
    });
  });
});
