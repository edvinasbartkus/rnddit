import { Navigation } from 'react-native-navigation';
import { registerScreens } from '../../src/screens';

describe('registerScreens()', () => {
  it('registers SubredditScreen', () => {
    Navigation.registerComponent = jest.fn();
    registerScreens(null, null);
    expect(Navigation.registerComponent).toHaveBeenCalledWith(
      'rnddit.SubredditScreen', expect.any(Function), null, null
    );

    expect(Navigation.registerComponent).toHaveBeenCalledWith(
      'rnddit.PostScreen', expect.any(Function), null, null
    );
  });
});
