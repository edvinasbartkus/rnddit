import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';
import { SubredditScreen } from '../../../src/screens/top';

import { Provider } from 'react-redux';
import { configureStore } from '../../../src/redux';

global.fetch = jest.fn(() => new Promise(resolve => resolve()));

describe('SubredditScreen', () => {
  it('renders view', () => {
    const view = renderer.create(
      <Provider store={configureStore()}>
        <SubredditScreen />
      </Provider>
    );
  });
});
