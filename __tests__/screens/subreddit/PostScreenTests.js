import React from 'react';
import { shallow, configure } from 'enzyme';
import ReactSixteenAdapter from 'enzyme/build/adapters/ReactSixteenAdapter';

import { PostScreen } from '../../../src/screens/subreddit/PostScreen';
import { configureStore } from '../../../src/redux';

import { WebView } from 'react-native';

global.fetch = jest.fn(() => new Promise(resolve => resolve()));
configure({ adapter: new ReactSixteenAdapter() });

jest.mock('WebView', () => 'WebView')

const POST = {
  item: {
    data: {
      permalink: '/r/aww'
    }
  }
};

describe('PostScreen screen', () => {
  it('renders WebView', () => {
    const wrapper = shallow(
      <PostScreen post={POST} store={configureStore()} />
    );

    expect(wrapper.length).toEqual(1);
    expect(wrapper.html()).toEqual('<WebView></WebView>');
    expect(wrapper.prop('source')).toEqual({ uri: 'https://www.reddit.com/r/aww' });
  });
});
