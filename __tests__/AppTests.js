import { Navigation } from 'react-native-navigation';

describe('Main app initialization', () => {
  it('starts tab based app', () => {
    Navigation.startTabBasedApp = jest.fn();
    require('../src/App');
    expect(Navigation.startTabBasedApp).toHaveBeenCalled();
  });
});
