import { searchText } from '../../src/utils/search';

describe('searchText', () => {
  it('should find post with text "doggo"', () => {
    const posts = [
      {
        data: { title: 'This is very nice doggo' }
      },
      {
        data: { title: 'This DOGGO is CapitalCase' }
      },
      {
        data: { title: 'DOGOGOGOGOGODO' }
      }
    ];

    const query = 'Doggo';

    expect(searchText(posts, query)).toEqual([
      {
        data: { title: 'This is very nice doggo' }
      },
      {
        data: { title: 'This DOGGO is CapitalCase' }
      },
    ]);
  });
});
